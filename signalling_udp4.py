import asyncio

directionClient = None
directionServer = None
dictForClientDirections = {}
counterClients = 0

class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        global directionServer, directionClient, dictForClientDirections, counterClients
        if message == "REGISTER SERVER" or message == "REGISTER CLIENT":
            if message == "REGISTER SERVER":
                directionServer = addr
                print('Received %r from %s' % (message, directionServer))
                print('Registering server, sending OK')
                answerForRegisterServer = "OK_Server"
                print('Send %r to %s' % (answerForRegisterServer, addr), "\n")
                self.transport.sendto(answerForRegisterServer.encode(), directionServer)
            if message == "REGISTER CLIENT":
                print('Received %r from %s' % (message, addr))
                print('Registering client, sending OK')
                directionClient = addr
                counterClients += 1
                dictForClientDirections[f'Client_{counterClients}'] = addr
                # for clientDataAddress in dictForClientDirections:  # THIS BLOCK IS FOR TESTING DIRECTION
                #    if dictForClientDirections[clientDataAddress] == addr:
                #        print(clientDataAddress, addr, f"\n {dictForClientDirections[clientDataAddress]}")
                answerForRegisterClient = "OK_Client"
                print('Send %r to %s' % (answerForRegisterClient, directionClient), "\n")
                self.transport.sendto(answerForRegisterClient.encode(), directionClient)
                if counterClients > 1:
                    print("Sending new client")
                    newClient = f"NEW_Client{counterClients}"
                    self.transport.sendto(newClient.encode(), directionServer)
        else:
            if "sdp" and "offer" in message:
                print("Client offer")
                print('Received %r from %s' % (message, addr))
                directionClient = addr
                print("Client Direction -> ", directionClient)
                self.transport.sendto(data, directionServer)
                print('Sending %r to %s' % (data, directionServer), "\n")
            if "sdp" and "answer" in message:
                print("Server answer")
                print('Received %r from %s' % (message, addr))
                directionServer = addr
                print("Server Direction -> ", directionServer)
                self.transport.sendto(data, directionClient)
                print('Sending %r to %s' % (data, directionClient), "\n")
            if "bye" in message:
                print(message)
                print('Received %r from %s' % (message, addr))
                for clientDataAddress in dictForClientDirections:
                    if addr == dictForClientDirections[clientDataAddress]:
                        self.transport.sendto(data, dictForClientDirections[clientDataAddress]) # Sending back bye to client
                print('Sending %r to %s' % (data, directionServer), "\n")
                self.transport.sendto(data, directionServer)


async def main():
    print("Starting UDP server")
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(), local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


if __name__ == "__main__":
    asyncio.run(main())
