import argparse
import asyncio
import logging
import math
import json
import cv2
import numpy
from aiortc import (
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
    RTCDataChannelParameters,
    VideoStreamTrack,
)
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder
from aiortc.contrib.signaling import BYE, add_signaling_arguments, create_signaling
from av import VideoFrame


class FlagVideoStreamTrack(VideoStreamTrack):
    """
    A video track that returns an animated flag.
    """

    def __init__(self):
        super().__init__()  # don't forget this!
        self.counter = 0
        height, width = 480, 640

        # generate flag
        data_bgr = numpy.hstack(
            [
                self._create_rectangle(
                    width=213, height=480, color=(255, 0, 0)
                ),  # blue
                self._create_rectangle(
                    width=214, height=480, color=(255, 255, 255)
                ),  # white
                self._create_rectangle(width=213, height=480, color=(0, 0, 255)),  # red
            ]
        )

        # shrink and center it
        M = numpy.float32([[0.5, 0, width / 4], [0, 0.5, height / 4]])
        data_bgr = cv2.warpAffine(data_bgr, M, (width, height))

        # compute animation
        omega = 2 * math.pi / height
        id_x = numpy.tile(numpy.array(range(width), dtype=numpy.float32), (height, 1))
        id_y = numpy.tile(
            numpy.array(range(height), dtype=numpy.float32), (width, 1)
        ).transpose()

        self.frames = []
        for k in range(30):
            phase = 2 * k * math.pi / 30
            map_x = id_x + 10 * numpy.cos(omega * id_x + phase)
            map_y = id_y + 10 * numpy.sin(omega * id_x + phase)
            self.frames.append(
                VideoFrame.from_ndarray(
                    cv2.remap(data_bgr, map_x, map_y, cv2.INTER_LINEAR), format="bgr24"
                )
            )

    async def recv(self):
        pts, time_base = await self.next_timestamp()

        frame = self.frames[self.counter % 30]
        frame.pts = pts
        frame.time_base = time_base
        self.counter += 1

        return frame

    def _create_rectangle(self, width, height, color):
        data_bgr = numpy.zeros((height, width, 3), numpy.uint8)
        data_bgr[:, :] = color
        return data_bgr


messageServer = None
okServer = ""
echoConnection = None
byeSignal = None
signalBreak = None
confirmedNewClient = None
newSDPClient = ""
conditionReset = None

class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None
        self.frameCount = 0

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        global confirmedNewClient, newSDPClient
        if data.decode() == "OK_Server":
            print("Connection established")
            global okServer
            okServer = data.decode()
        if "sdp" and "offer" in data.decode():
            global messageServer
            messageServer = data.decode()
            if confirmedNewClient is True:
                newSDPClient = data.decode()
        if "bye" in data.decode():
            global byeSignal
            byeSignal = True
            messageServer = data.decode()

        if "NEW_Client" in data.decode():
            confirmedNewClient = True



    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def waitRemoteDescription(pc):
    while not pc.remoteDescription is None:
        await asyncio.sleep(1)


async def waitNewRemoteDescription(pc):
    while pc.remoteDescription is None:
        await asyncio.sleep(1)


async def waitBreak():
    while not signalBreak:
        await asyncio.sleep(1)


async def waitForBye():
    while not byeSignal:
        await asyncio.sleep(1)


def resetVariables():
    global messageServer, okServer, echoConnection
    messageServer = None
    okServer = ""
    # newSDPClient = ""

async def runNewClients(pc, player, recorder):
    global messageServer, echoConnection
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    messageServer = "New clients"
    # print("Server Registering...")
    echoConnection = EchoClientProtocol(messageServer, on_con_lost)
    await loop.create_datagram_endpoint(lambda: echoConnection, remote_addr=('127.0.0.1', 9999))
    # await loop.create_datagram_endpoint(lambda: echoConnection, remote_addr=('127.0.0.1', 9999))

    def add_tracks():
        if player and player.audio:
            pc.addTrack(player.audio)

        if player and player.video:
            pc.addTrack(player.video)
        else:
            pc.addTrack(FlagVideoStreamTrack())

    @pc.on("track")
    def on_track(track):
        print("Receiving %s" % track.kind)
        recorder.addTrack(track)

        async def framesCounting():
            nonlocal track
            while True:
                await track.recv()
                echoConnection.frameCount += 1
                print("Frame:", echoConnection.frameCount)
                if echoConnection.frameCount == 146: # 146 value in case of any packet is lost
                    print(f"Limit number of frames reached -> {echoConnection.frameCount} \n Terminating connection...")
                    break

            await waitForBye()
            print("Bye received from client")
            await recorder.stop()
            await connectSignaling(pc)

        asyncio.ensure_future(framesCounting())

    async def connectSignaling(pc):
        global messageServer, newSDPClient, echoConnection
        while True:
            await asyncio.sleep(4)
            if confirmedNewClient is True:
                offerDict = json.loads(newSDPClient)
                offerSDP = offerDict['sdp']
                offerSDPType = offerDict['type']
                offerAnswer = RTCSessionDescription(offerSDP, offerSDPType)
                if isinstance(offerAnswer, RTCSessionDescription):
                    await pc.setRemoteDescription(offerAnswer)
                await recorder.start()
                add_tracks()
                await pc.setLocalDescription(await pc.createAnswer())
                answer = pc.localDescription
                messageServer = json.dumps(answer.__dict__)
                echoConnection = EchoClientProtocol(messageServer, on_con_lost)
                await loop.create_datagram_endpoint(lambda: echoConnection, remote_addr=('127.0.0.1', 9999))
                await waitRemoteDescription(pc)

            if "bye" in messageServer:
                print("Resetting variables")
                resetVariables()
                echoConnection.frameCount = 0
                pc = RTCPeerConnection()
                await run(
                    pc=pc,
                    player=player,
                    recorder=MediaRecorder(args.record_to),
                    signaling=signaling,
                    role=args.role,
                )

    await connectSignaling(pc)


async def run(pc, player, recorder, signaling, role):
    global messageServer, echoConnection
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    messageServer = "REGISTER SERVER"
    print("Server Registering...")
    echoConnection = EchoClientProtocol(messageServer, on_con_lost)
    await loop.create_datagram_endpoint(lambda: echoConnection, remote_addr=('127.0.0.1', 9999))

    def add_tracks():
        if player and player.audio:
            pc.addTrack(player.audio)

        if player and player.video:
            pc.addTrack(player.video)
        else:
            pc.addTrack(FlagVideoStreamTrack())

    @pc.on("track")
    def on_track(track):
        print("Receiving %s" % track.kind)
        recorder.addTrack(track)

        async def framesCounting():
            nonlocal track
            while True:
                await track.recv()
                echoConnection.frameCount += 1
                print("Frame:", echoConnection.frameCount)
                if echoConnection.frameCount == 146: # 146 value in case of any packet is lost
                    print(f"Limit number of frames reached -> {echoConnection.frameCount} \n Terminating connection...")
                    break

            await waitForBye()
            print("Bye received from client")
            await recorder.stop()
            await connectSignaling(pc, echoConnection)
        asyncio.ensure_future(framesCounting())

    async def connectSignaling(pc, echo):

        global messageServer, newSDPClient, conditionReset
        while True:
            await asyncio.sleep(4)
            if "sdp" and "offer" in messageServer:
                offerDict = json.loads(messageServer)
                offerSDP = offerDict['sdp']
                offerSDPType = offerDict['type']
                offerAnswer = RTCSessionDescription(offerSDP, offerSDPType)
                if isinstance(offerAnswer, RTCSessionDescription):
                    await pc.setRemoteDescription(offerAnswer)
                await recorder.start()
                add_tracks()
                await pc.setLocalDescription(await pc.createAnswer())
                answer = pc.localDescription
                messageServer = json.dumps(answer.__dict__)
                echo.transport.sendto(messageServer.encode())
                print("Send:", messageServer)
                await waitRemoteDescription(pc)

            if "sdp" and "offer" in newSDPClient: # Code for new Clients after first
                # pc = RTCPeerConnection()
                # offerDict = json.loads(newSDPClient)
                # offerSDP = offerDict['sdp']
                # offerSDPType = offerDict['type']
                # offerAnswer = RTCSessionDescription(offerSDP, offerSDPType)
                # if isinstance(offerAnswer, RTCSessionDescription):
                #    await pc.setRemoteDescription(offerAnswer)
                # await recorder.start()
                # add_tracks()
                print("PARADA")
                # await waitNewRemoteDescription(pc)
                # await pc.setLocalDescription(await pc.createAnswer())
                # answer = pc.localDescription
                # messageServer = json.dumps(answer.__dict__)
                # echo.transport.sendto(messageServer.encode())
                # print("Send:", messageServer)
                # await asyncio.sleep(2)
                # resetVariables()
                # echo.frameCount = 0
                conditionReset = True

            if conditionReset is True:
                print("Resetting variables")
                resetVariables()
                echo.frameCount = 0
                pc = RTCPeerConnection()
                await runNewClients(
                    pc=pc,
                    player=player,
                    recorder=MediaRecorder(args.record_to),
                    )

    await connectSignaling(pc, echoConnection)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("--role", default='answer')
    parser.add_argument("--play-from", help="Read the media from a file and sent it."),
    parser.add_argument("--record-to", help="Write received media to a file.", default="video-out.mp4"),
    parser.add_argument("--verbose", "-v", action="count")
    add_signaling_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    # create signaling and peer connection
    signaling = create_signaling(args)
    pc = RTCPeerConnection()

    # create media source
    if args.play_from:
        player = MediaPlayer(args.play_from)
    else:
        player = None

    # create media sink
    if args.record_to:
        recorder = MediaRecorder(args.record_to)
    else:
        recorder = MediaBlackhole()

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            run(
                pc=pc,
                player=player,
                recorder=recorder,
                signaling=signaling,
                role=args.role,
            )
        )
    except KeyboardInterrupt:
        pass
    finally:
        # cleanup
        loop.run_until_complete(recorder.stop())
        loop.run_until_complete(signaling.close())
        loop.run_until_complete(pc.close())
